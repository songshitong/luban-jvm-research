package com.luban.ziya.runengine;

public class StackAlloc {

    int a = 10;

    public static void main(String[] args) {
        long start = System.currentTimeMillis();

        for (int i = 0; i < 100000; i++) {
            StackAlloc obj = new StackAlloc();
        }

        long end = System.currentTimeMillis();

        System.out.println((end - start) + " ms");

        while (true);
    }

    public static void alloc() {
        StackAlloc obj = new StackAlloc();
    }
}
