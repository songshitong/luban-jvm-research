package com.luban.ziya.jmm;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created By ziya
 * 2020/12/1
 */
public class Test_1 {

    public static void main(String[] args) {
//        Thread thread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    TimeUnit.SECONDS.sleep(Integer.MAX_VALUE);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//
//        thread.setDaemon(false);
//        thread.start();

        Runtime.getRuntime().addShutdownHook(new Thread(()->{
            while (true){
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }));

        System.out.println("hello");
    }

    public void t1() {
        t2();
    }

    public static void t2() {
        System.out.println("s2");
    }
}
